module.exports = {
	name: 'ping',
	description: 'Answer pong',
	args: false,
	usage:'ping',
	execute(msg) {
		msg.channel.send('Pong.');
	},
};
