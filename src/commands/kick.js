module.exports = {
	name: 'kick',
	description: 'Kick the given nickname',
	args: true,
	usage:'kick <nickname>',
	execute(msg, args) {
		if (!args.length) {
			return msg.channel.send(`Hey ${msg.author}! You didn't tell me the guy you wanted to kick !`);
		}
		else if (args[0] === 'foo') {
			return msg.channel.send('bar');
		}
		msg.channel.send('Server name : ' + msg.guild.name + '\nTotal members: ' + msg.guild.memberCount + ' ' + args);
	},
};
