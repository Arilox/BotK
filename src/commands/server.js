module.exports = {
	name: 'server',
	description: 'Gives the server informations',
	args: false,
	usage:'server',
	execute(msg, args) {
		msg.channel.send('Server name : ' + msg.guild.name + '\nTotal members: ' + msg.guild.memberCount + ' ' + args);
	},
};
