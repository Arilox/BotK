# Table des matières

1. [Lexique](#Lexique)
   1. [Fichier](#Fichier)
   2. [Extension de fichier](#Extension de fichier)
   3. [Programme](#Programme)
   4. [Logiciel](#Logiciel)
   5. [Réseau](#Réseau)
   6. [Internet](#Internet)
   7. [Élement graphique](#Élement graphique)
   8. [Site web](#Site web)
   9. [Site web dynamique](#Site web dynamique)
   10. [Lien](#Lien)
   11. [Navigateur](#Navigateur)
2. 



# Lexique

Ce lexique n’est pas classé par ordre alphabéthique enfin de pouvoir être comme un cours et comprendre au fur et à mesure.



## Fichier

Ensemble de données possédant un nom et une extention.



## Extension de fichier

L’extention suit le nom du fichier et permet souvent de déterminer le type de ses données.

Exemple : Un fichier  recette.txt correspondent à du texte, alors que les fichier .png, .jpeg correspondront à des images.



## Dossier

Aussi appelé répertoire, est un fichier qui contient une liste de fichiers. Un dossier peut donc contenir d’autres dossiers puisque ceux ci sont eux même des fichier. Il permet d’organiser les fichier dans une [arborescence](https://fr.wikipedia.org/wiki/Arborescence).



## Chemin de fichier

Le chemin d’un fichier est le nom du stockage sur lequel il est stocké suivi du nom des dossier par lesquels il faut passer pour arriver au fichier correspondant au nom affiché.

Exemple : USB:/Dossier1/Dossier2/…/DossierX/fichier.extension

## Support de stockage numérique

Tout objet physique permettant de stocker des fichier, comme un disque dur, un SSD, une clé USB… Il est la racine de l’arborescence



## Explorateur de fichier

L’explorateur de fichier permet d’affichier 



## Programme

Ensembles d’opérations effectuables sur des données.



## Logiciel

Ensemble de programmes et de fichier contenant des données utilisable dans ces programmes.



## Réseau

Ensemble d’ordinateurs connectés et partageant des données entre eux.



## Internet

Réseau accessible au public.



## Serveur

Ordinateur partageant des données sur un réseau, accessible par n’importe quel ordinateur connecté à ce réseau.



## Adresse IP

Tout ordinateur connecté à un réseau posséde une adresse IP qui permet de l’identifier et de le retrouver facilement. 

Les adresses IPv4 vont de 0.0.0.0 à 255.255.255.255

Exemple : 192.102.1.125 ou 187.12.2.125



## Élement graphique

Tout texte, image, et vidéo, qui peut être afficher sur un écran.



## Navigateur

Logiciel permettant d’afficher des fichiers, généralement des pages internet.



## Page web

Une page web contient des elements graphiques hiérarchisé par titres, paragraphes, etc… (HTML)

Les éléments graphiques d’une page peuvent être affichés différement selon les sites et parfois en fonction de la taille de l’écran ou de la fenêtre. (CSS)



## Site web

Un site web contient plusieurs pages web, c’est un peu un livre numérique, sauf que les pages n’ont pas forcement d’ordre. 

On y accéde généralement aux différentes pages par un **index** qui est une sorte de table des matières et généralement la page principale par laquelle on accède au site.



## Site web dynamique

Un site web dynamique peut générer de nouvelles pages web ou de nouveaux éléments graphiques en fonction de ce que l’utilisateur tape ou clique.



## Adresse web

https://fr.wikipedia.org/wiki/Adresse_web est un exemble d’adresse web.

`http://` signifie que la page est stockée sur un serveur, donc sur internet

`https://` signifie que la page est stockée sur un serveur sécurisé. Vous devez toujours vérifier qu’une page internet sur laquelle vous rentrez des informations personnelles sensibles tel qu’une carte bancaire soit précédé de ce préfixe.

`fr.wikipedia.org` est le nom de domaine

`/wiki/Adresse_Web` est le chemin de la page affichée sur le serveur



## Nom de domaine

Un nom de domaine à un nom semblable aux fichier, sauf qu’il est lié à l’adresse IP d’un serveur.

Le nom de domaine www.google.com mènera à l’adresse IP 216.58.208.238.

Entrer l’un ou l’autre dans la barre de recherche de son navigateur mènera au même site web.



## Lien

Adresse vers une page web fixée sur un élement graphique, accesible en cliquant sur cet élement.



